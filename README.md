
## READ BEFORE USE

This C# script file is intended to use for **UnityEngine** projects,  
allowing to use custom methods on various **object**s and **struct**s, with some  
specific to **UnityEngine**.  
These methods are not meant to be the most optimized on memory allocation  
or CPU usage, but are a compromise between optimization and fast writing  
of the code.
