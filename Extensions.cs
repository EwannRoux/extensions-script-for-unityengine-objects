﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

//Theses methods are not meant to be optimised, but to be used for ergonomy.
public static partial class Extensions {

    /// <summary>
    /// Return an array of length 1, using the given parameter.
    /// </summary>
    /// <typeparam name="T">The Type of the value. Often unnecessary, since the IDE will most likely determine it itself.</typeparam>
    /// <param name="value">The value to put in the array.</param>
    /// <returns>The array newly created.</returns>
    public static T[] ToMonoArray<T>(this T value) {
        return new T[] { value };
    }

    /// <summary>
    /// Crate an array of transform from an array of MonoBehaviour.
    /// </summary>
    /// <param name="array"></param>
    /// <returns>An array of transform, which indexes correspond to thoses of the parameter array. </returns>
    public static Transform[] ToTransformArray(this MonoBehaviour[] array) {
        //Create an array of transform, with a size equals to the provided array.
        Transform[] transformArray = new Transform[array.Length];

        //We iterate through the array
        for (int i = 0; i < array.Length; i++) {
            //... and get the cached transform reference.
            transformArray[i] = array[i].transform;
        }
        return transformArray;
    }



    /// <summary>
    /// Calculate the distance between each successive points from the given array. Usefull to calculate the lenght of a NavMeshPath, perimeter of complex shape, etc.
    /// </summary>
    /// <param name="pathPoints">The path of successive point to measure.</param>
    /// <param name="throwExceptionOnInvalidPath">Set as true if return exception on array if array has an invalid lenght,false to return 0 instead.</param>
    /// <returns>The calculated distance.</returns>
    /// <returns></returns>
    public static float SumOfLenghtsBetweenSuccessivePoint(this Vector3[] pathPoints, bool throwExceptionOnInvalidPath = false) {

        //If we must throw an exception, we check if the lenght is valid.
        if (throwExceptionOnInvalidPath) {
            if (pathPoints.Length < 2) {
                throw new Exception("Array lenght is less than 2, array lenght is:" + pathPoints.Length);
            }
        }
        //The measured lenght, the variable that will be returned.
        float lenght = 0;

        //If path is invalid, we return the lenght before trying to enter the for.
        if (pathPoints.Length < 2) {
            return lenght;
        }

        //For each points, except the last, read its value and the value of the next point, and add the distance to the lenght var.
        for (int i = 0; i < pathPoints.Length - 1; ++i) {
            lenght += Vector3.Distance(pathPoints[i], pathPoints[i + 1]);
        }

        return lenght;
    }

    /// <summary>
    /// Get the value of an angle in a range of [-180,180]. The main purpose is to "reset" the euleur angles of a transform before doing a lerp on its euleur angle, to prevent any fast spin during the lerp (wich maybe happen when there is value like  800 degrees).
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    public static int ClampAngleBetween180Deg_NegativeAndPositive(this int angle) {

        if (angle > 180) {
            //For example, 190° degree, is equal to -170°, hence we substract 360 from the angle value, like  this:
            //return angle - 360;
            //But sometime, angle can be of a large value, so substracting 360 may be not enought, so we need to substract again, until angle is in our range.
            do {
                angle -= 360;
            } while (angle > 180);
        } else if (angle < -180) {
            //Same as previous, but this time we add 360.
            do {
                angle += 360;
            } while (angle > 180);
        }
        return angle;
    }

    /// <summary>
    /// Get the value of an angle in a range of [-180,180]. The main purpose is to "reset" the euleur angles of a transform before doing a lerp on its euleur angle, to prevent any fast spin during the lerp (wich maybe happen when there is value like  800 degrees).
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    public static float ClampAngleBetween180NegAndPos(this float angle) {
        if (angle > 180) {
            //For example, 190° degree, is equal to -170°, hence we substract 360 from the angle value, like  this:
            //return angle - 360;
            //But sometime, angle can be of a large value, so substracting 360 may be not enought, so we need to substract again, until angle is in our range.
            do {
                angle -= 360;
            } while (angle > 180);
        } else if (angle < -180) {
            //Same as previous, but this time we add 360.
            do {
                angle += 360;
            } while (angle > 180);
        }
        return angle;
    }

    /// <summary>
    /// Shothand to apply ClampAngleBetween180NegAndPos() to the y parameter  of the eulerAngle of a transform.
    /// </summary>
    /// <param name="transform"></param>
    public static void NormalizeEulerAnglesY(this Transform transform, bool clampLocaly = false) {
        if (clampLocaly) {
            transform.localEulerAngles = transform.localEulerAngles.SetY(transform.eulerAngles.y.ClampAngleBetween180NegAndPos());
            return;
        }
        transform.eulerAngles = transform.eulerAngles.SetY(transform.eulerAngles.y.ClampAngleBetween180NegAndPos());
    }



    /// <summary>
    /// Return 1 if number is 0 or negative, return 1, else return current value.
    /// </summary>
    /// <param name="baseFloat">The initial value.</param>
    /// <returns>Positive value, either 1 or current.</returns>
    public static float ForcePositive(this float baseFloat) {
        return baseFloat <= 0 ? 1 : baseFloat;
    }

    /// <summary>
    /// Return 1 if number is 0 or negative, return 1, else return current value.
    /// </summary>
    /// <param name="baseInt">The initial value.</param>
    /// <returns>Positive value, either 1 or current.</returns>
    public static int ForcePositive(this int baseInt) {
        return baseInt <= 0 ? 1 : baseInt;
    }


    /// <summary>
    /// Calculate the angle (to use as  [z] axis) between the RectTransform and the TargetRectTransform. Used to allign visual element between them, such as arrows.
    /// </summary>
    /// <param name="rectTransform">The initial RecTransform.</param>
    /// <param name="otherRectTransform">The targeted RectTransform.</param>
    /// <returns>Value to use as [z] component of the eulerAgnles of a Transform.</returns>
    public static float CanvasSpaceAngleBetweenRightAndOtherRectTransform(this RectTransform rectTransform, RectTransform otherRectTransform) {
        return Vector2.SignedAngle(Vector3.right, (Vector2)otherRectTransform.position - (Vector2)rectTransform.position);
    }

    /// <summary>
    /// Calculate the angle (to use as  [z] axis) between the RectTransform and the TargetRectTransform. Used to allign visual element between them, such as arrows.
    /// </summary>
    /// <param name="rectTransform">The initial RecTransform.</param>
    /// <param name="posV3">The targeted RectTransform.</param>
    /// <returns>Value to use as [z] component of the eulerAgnles of a Transform.</returns>
    public static float AngleBetweenTwoPointInScreenSpaceCanvas(this RectTransform rectTransform, Vector3 posV3) {
        return Vector2.SignedAngle(Vector3.right, (Vector2)posV3 - (Vector2)rectTransform.position);
    }

    /// <summary>
    /// Get a string from a DateTime that can be used as a file name.Format is "prefix"+YYYY-MM-DD+"Suffix". 
    /// /!\There is no check applied to the suffix and the prefix to prevent invalid char in file name.
    /// </summary>
    /// <param name="dateTime"></param>
    /// <param name="prefix"></param>
    /// <param name="suffix"></param>
    /// <returns></returns>
    public static string ToStringForFileName(this System.DateTime dateTime, string prefix, string suffix) {


        System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder(prefix, prefix.Length + 20+suffix.Length);
        stringBuilder.AppendFormat("{0}{1}-{2}-{3}-{4}-{5}{6}", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, suffix);


        if (dateTime.Month.ToString().Length == 1) {
            stringBuilder.Insert(prefix.Length + 4, "0");
        }
        if (dateTime.Day.ToString().Length == 1) {
            stringBuilder.Insert(prefix.Length + 7, "0");
        }
        if (dateTime.Hour.ToString().Length == 1) {
            stringBuilder.Insert(prefix.Length + 10, "0");
        }
        if (dateTime.Minute.ToString().Length == 1) {
            stringBuilder.Insert(prefix.Length + 13, "0");
        }
        if (dateTime.Second.ToString().Length == 1) {
            stringBuilder.Insert(prefix.Length + 16, "0");
        }

        return stringBuilder.ToString();
    }

    /// <summary>
    /// PlayerPrefs can only store string, interger and float. This method allow simple convertion from bool to int to prevent any confusion when setting PlayerPrefs values.
    /// </summary>
    /// <param name="boolean"></param>
    /// <returns></returns>
    public static int ToZeroOrOne(this bool boolean) {
        if (boolean) {
            return 1;
        }
        return 0;
    }

    /// <summary>
    /// PlayerPrefs can only store string, interger and float. This method allow simple convertion from int to bool to prevent any confusion when getting PlayerPrefs values.
    /// </summary>
    /// <param name="integer"></param>
    /// <returns></returns>
    public static bool FromZeroToFalse(this int integer) {
        if (integer == 0) {
            return false;
        }
        return true;
    }



    /// <summary>
    /// Add empty range to a collection, for example to
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection">The collection that will recieve an empty range.</param>
    /// <param name="rangeSize"></param>
    public static void AddEmptyRange<T>(this List<T> collection, int rangeSize) where T : class {
        //negative or 0 range size are invalid, so we ignore it
        if (rangeSize < 1) {
            return;
        }
        //we add as many null index as required
        for (int i = 0; i < rangeSize; i++) {
            collection.Add(null);
        }
    }

    /// <summary>
    /// Insert a collection at the given index of the current collection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <param name="toAdd">The collection to insert.</param>
    /// <param name="insertIndex">The index where the new collection must be inserted.</param>
    public static void InsertRangeFromIndex<T>(this List<T> collection, List<T> toAdd, int insertIndex) {

        if(insertIndex < 0) {
            throw new IndexOutOfRangeException();
        }
        
        //if index required is greater or equal than collection count, the prebuild AddRange can be used instead of doing this custom insert
        if (insertIndex >= collection.Count) {
            collection.AddRange(toAdd);
        } else { //we will ned to manipulate the collection

            int lenghtOfFinalCollection = collection.Count + toAdd.Count;
            List<T> newCollection = new List<T>(lenghtOfFinalCollection);

            //add pre-insert index values from original collection to new collection           
            for (int currentIndex = 0; currentIndex < insertIndex; currentIndex++) {
                newCollection.Add(collection[currentIndex]);
            }

            //add the collection to insert just after the "pre-insert range"
            for (int currentIndex = 0; currentIndex < toAdd.Count; currentIndex++) {
                newCollection.Add(toAdd[currentIndex]);
            }

            //add post-insert index to new collection
            for (int currentIndex = insertIndex; currentIndex < collection.Count; currentIndex++) {
                newCollection.Add(collection[currentIndex]);
            }

            //replace previous collection with new collection
            collection = newCollection;
        }
    }


 


    /// <summary>
    /// Return an array of the current collection, merged with the params collections, in the order given in parameters. Simulate the use of multiple .AddRange(collection) of List, but for Array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <param name="collections">All the collections to merge with the current collection.</param>
    /// <returns></returns>
    public static T[] ReturnMergedCollection<T>(this T[] collection, params T[][] collections) {

        //The total length required to create the fully merged collection.
        //We initialise this value with the lenght of the current collection
        int lenght = collection.Length;
        for (int i = 0; i < collections.Length; i++) {
            lenght += collections[i].Length;
        }

        //We create the new collection using the total length required.
        T[] newCollection = new T[lenght];

        //We iterate through the initial collection, then through all the params collections.
        for (int j = 0; j < collection.Length; j++) {
            newCollection[j] = (collection[j]);
        }

        //We need an index that will be incremented in the iterations, to know wich index of the merged collection must be populated.
        int currentIndex = collection.Length;

        for (int i = 0; i < collections.Length; i++) {
            for (int j = 0; j < collections[i].Length; j++) {
                newCollection[currentIndex] = collections[i][j];
                currentIndex++;
            }
        }

        return newCollection;

    }

    /// <summary>
    /// Return an array of the current collection, merged with the params collections, in the order given in parameters. Use multiple .AddRange(collection) to create and return a new List.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <param name="collections"></param>
    /// <returns></returns>
    public static List<T> MergeCollection<T>(List<T> collection, params List<T>[] collections) {

        //The total length required to create the fully merged collection.
        //We initialise this value with the lenght of the current collection
        int lenght = collection.Count;
        for (int i = 0; i < collections.Length; i++) {
            lenght += collections[i].Count;
        }

        List<T> newCollection = new List<T>(lenght);
        for (int j = 0; j < collection.Count; j++) {
            newCollection[j] = (collection[j]);
        }

        for (int i = 0; i < collections.Length; i++) {            
                newCollection.AddRange(collections[i]);           
        }

        return newCollection;      
    }

    public static void DebugLogAllValues<T>(this T[] array) {
        for (int i = 0; i < array.Length; i++) {
            Debug.Log(array[i]);
        }
    }



    /// <summary>
    /// Return a copy of the array, with any occurence of the inputed value removed. Array size is different.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="array"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T[] Remove<T>(this T[] array, T value) {
        T[] returnArray = new T[array.Length - 1];
        int index = 0;
        for (int i = 0; i < array.Length; i++) {
            if (array[i].Equals(value)) {
                continue;
            }

            returnArray[index] = array[i];
            index++;
        }
        return returnArray;
    }

    /// <summary>
    /// Return a copy of a new array, with the required index removed. Array size is different.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="array"></param>
    /// <param name="indexToRemove"></param>
    /// <returns></returns>
    public static T[] RemoveAtIndex<T>(this T[] array, int indexToRemove) {

        T[] returnArray = new T[array.Length - 1];
        int index = 0;
        for (int i = 0; i < array.Length; i++) {
            if (i == indexToRemove) {
                continue;
            }
            returnArray[index] = array[i];
            index++;
        }
        return returnArray;
    }


    public static void Shuffle<T>(this IList<T> list) {

        int listLenght = list.Count;
        
        for (int indexToShuffle = 0; indexToShuffle < listLenght-1 /*we must not include the last index in the iteration*/; indexToShuffle++) {
            //Setting indexToShuffle as the minimun value of the random number
            //prevent the current index to be shuffled with an index already iterated.
            //Last index is prevented from iterating bc the random value will be itself.
            //This variable is the index with wich the current value will be swaped.
            int indexToSwapWith = Random.Range(indexToShuffle, listLenght);

            //We store the current value of the target index in a temp variable, then use it to swap the values of the two indexes.
            T tempValueOfOtherIndex = list[indexToSwapWith];
            list[indexToSwapWith] = list[indexToShuffle];
            list[indexToShuffle] = tempValueOfOtherIndex;
        }
    }

    /// <summary>
    /// Return a copy of the array, with values in reverse order.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <returns></returns>
    public static T[] ReversedCopy<T>(this T[] collection) {
        T[] arrayToReturn = new T[collection.Length];

        for (int i = collection.Length - 1; i >= 0; i--) {
            arrayToReturn[i] = collection[i];
        }
        return arrayToReturn.ToArray();
    }

    /// <summary>
    /// Return a copy of the list, with values in reverse order.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <returns></returns>
    public static List<T> ReversedCopy<T>(List<T> collection) {
        List<T> listToReturn = new List<T>(collection.Count);

        for (int i = collection.Count - 1; i >= 0; i--) {
            listToReturn.Add(collection[i]);
        }
        return listToReturn;
    }


    /// <summary>
    /// Return the last value of the collection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <returns></returns>
    public static T LastValue<T>(this IEnumerable<T> collection) {
        return collection.Last<T>();
    }


    public static float RoundToDecimal(this float baseValue, int numberOfDecimalToKeep, bool debug = false) {

        //Throw exception if asking for a negative decimal count.
        if(numberOfDecimalToKeep < 0) {
            throw new ArithmeticException();
        }

        //Instead of doing something like 
        //
        //  //float coeff = Mathf.pow(10.0f, (float)numberOfDecimalToKeep);
        //  //return Mathf.Round(value * coeff) / coeff;
        //
        //We separate the decimal part from the non decimal part of the float, an operate only on it, to prevent  floating point imprecision errors.
        float decimalPartOfBaseValue = baseValue % 1;
        float coeff = Mathf.Pow(10, numberOfDecimalToKeep);
        return Mathf.Floor(baseValue) + (Mathf.Round(decimalPartOfBaseValue * coeff) / coeff);


        //Previously used variables before reduction of local variables usage.
        
        //float nonDecimalPartOfBaseValue = Mathf.Floor(baseValue);
        //float decimalPartRoundedToNDecimals = Mathf.Round(decimalPartOfBaseValue * coeff) / coeff;
        //return nonDecimalPartOfBaseValue + decimalPartRoundedToNDecimals;
    }

    /// <summary>
    /// Shortand to return a copy of a Color struct, with only one color edited. 
    /// </summary>
    /// <param name="color"></param>
    /// <param name="value">Value, from 0 to 1.</param>
    /// <returns></returns>
    public static Color SetRed(this Color color, float value) {
        color.r = Mathf.Clamp01(value);
        return color;
    }

    /// <summary>
    /// Shortand to return a copy of a Color struct, with only one color edited. 
    /// </summary>
    /// <param name="color"></param>
    /// <param name="value">Value, from 0 to 1.</param>
    /// <returns></returns>
    public static Color SetGreen(this Color color, float value) {
        color.g = Mathf.Clamp01(value);
        return color;
    }

    /// <summary>
    /// Shortand to return a copy of a Color struct, with only one color edited. 
    /// </summary>
    /// <param name="color"></param>
    /// <param name="value">Value, from 0 to 1.</param>
    /// <returns></returns>
    public static Color SetBlue(this Color color, float value) {
        color.b = Mathf.Clamp01(value);
        return color;
    }

    /// <summary>
    /// Shortand to return a copy of a Color struct, with only the alpha edited. 
    /// </summary>
    /// <param name="color"></param>
    /// <param name="value">Value, from 0 to 1.</param>
    /// <returns></returns>
    public static Color SetAlpha(this Color color, float value) {
        color.a = Mathf.Clamp01(value);
        return color;
    }

    /// <summary>
    /// Ignoring the y axis, calculate the squared value of the distance between the two points. sqrMagnitude is faster than magnitude, so this can be used to compare ("<" or ">") the squared distance of theses two points against a stored squared distance.
    /// </summary>
    /// <param name="local"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static float HorizontalSquaredDistance(this Vector3 local, Vector3 other) {
        Vector2 local2D = new Vector2(local.x, local.z);
        Vector2 other2D = new Vector2(other.x, other.z);
        return (local2D - other2D).sqrMagnitude;
    }
    /// <summary>
    /// Ignoring the y axis, calculate the squared value of the distance between the two points.
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <returns></returns>
    public static float HorizontalDistance(Vector3 point1, Vector3 point2) {
        point1.y = point2.y = 0;
        return Vector3.Distance(point1, point2);
    }


    /// <summary>
    /// Calculate the squared value of the distance between the two points. sqrMagnitude is faster than magnitude, so this can be used to compare ("<" or ">") the squared distance of theses two points against a stored squared distance.
    /// </summary>
    /// <param name="local"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public static float SquaredDistance(this Vector3 local, Vector3 other) {
        return (local - other).sqrMagnitude;
    }

   
    

    /// <summary>
    /// Return the signed distance, on y axis between the current Vector3, and the target Vector3.
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <returns></returns>
    public static float DistanceFromToVertical(this Vector3 point1, Vector3 point2) {
        return point1.y - point2.y;
    }



    /// <summary>
    /// Return the unsigned distance, on y axis between the current Vector3, and the target Vector3.
    /// </summary>
    /// <param name="point1"></param>
    /// <param name="point2"></param>
    /// <returns></returns>
    public static float DistanceVertical(this Vector3 point1, Vector3 point2) {
        return Mathf.Abs( point1.y - point2.y);
    }


   

    /// <summary>
    /// Return a copy of the Vector3, with only the x parameter modified.
    /// </summary>
    public static Vector3 SetX(this Vector3 vector, float X) {
        vector.x = X;
        return vector;
    }
   
    /// <summary>
    /// Return a copy of the Vector3, with only the y parameter modified.
    /// </summary>   
    public static Vector3 SetY(this Vector3 vector, float Y) {
        vector.y = Y;
        return vector;
    }

    /// <summary>
    /// Return a copy of the Vector3, with only the z parameter modified.
    /// </summary>  
    public static Vector3 SetZ(this Vector3 vector, float Z) {
        vector.z = Z;
        return vector;
    }



    /// <summary>
    /// Return a copy of the Vector2, with only the x parameter modified.
    /// </summary>
    public static Vector2 SetX(this Vector2 vector, float X) {
        vector.x = X;
        return vector;
    }

    /// <summary>
    /// Return a copy of the Vector2, with only the y parameter modified.
    /// </summary>
    public static Vector2 SetY(this Vector2 vector, float Y) {
        vector.y = Y;
        return vector;
    }


    /// <summary>
    /// Return a Vector3 based on the current Vector2, with the current X and Y as X and Z. Typically used to convert a Vector2 from an input callback to a Vector usable to move a character.
    /// </summary>
    public static Vector3 Vector2ToVector3XZ(this Vector2 vector2) {

        Vector3 vector3 = new Vector3();
        vector3.x = vector2.x;
        vector3.y = 0;
        vector3.z = vector2.y;
        return vector3;

    }

    /// <summary>
    /// Return av Vector2 based on the current Vector3, with the current X and Z as X and Y. Main use is to convert world position of a character into
    /// coordinate of a sprite in a minimap.
    /// </summary>
    public static Vector2 Vector3XZToVector2(this Vector3 vector3) {

        Vector2 vector2 = new Vector2();
        vector2.x = vector3.x;
        vector2.y = vector3.z;
        return vector2;
    }



    /// <summary>
    /// Return a Quaternion, which can make a transform looking at the direction of the target position. The quaterion make the transform rotate only on the Y axis,
    /// in other word, it acts like if the target is on the same plane as the transform.position.
    /// <param name="target">The point coordinate to look at.</param>
    /// </summary>
    public static Quaternion LookAtY(this Transform transform, Vector3 target) {
        return Quaternion.LookRotation(target.SetY(transform.position.y) - transform.position, Vector3.up);
    }

    /// <summary>
    /// Return a Quaternion, which can make a transform looking at the direction of the target position. The quaterion make the transform rotate only on the Y axis,
    /// in other word, it acts like if the target is on the same plane as the transform.position.
    /// </summary>
    /// <param name="target">The point coordinate to look at.</param>
    /// <param name="yMin">The minimal value of the y angle (vertical axis) of the rotation.</param>
    /// <param name="yMax">The maximal value of the y angle (vertical axis) of the rotation.</param>
    /// <returns></returns>
    public static Quaternion LookAtYClamped(this Transform transform, Vector3 target, float yMin, float yMax) {


        //Get a result similar to LookAtY();
        Quaternion quat = Quaternion.LookRotation(target.SetY(transform.position.y) - transform.position, Vector3.up);
       
        //Convert it to vector 3 rotation, in degrees.
        Vector3 eulers = quat.eulerAngles;

        //if yMin < 0, the call may probably use a range from ]-180; 180] instead of [0; 360[
        if (yMin < 0) {

            //make yMax greated than yMin, and lower than 180
            yMax = Mathf.Clamp(yMax, yMin+1, 180);

            //In this case, if eulers.y is in the ]180;360[ range, it must be converted in its equivalent in the ]-180; 0[ range.
            if (eulers.y > 180) {
                //Since a Quatertion.eulerAngles will always return angles values in the range [0;360[, there is not need to check if the result is still highter than 180
                eulers.y -= 360;
            }
        }

        //clamp the angle between the min and max values.
        eulers.y = Mathf.Clamp(eulers.y, yMin, yMax);

        //reassing the eulers angles of the quaternion with the clamped Y.
        quat.eulerAngles = eulers;
        return quat;
    }

    /// <summary>
    /// Return a Vector3, which can make a transform looking at the direction of the target position. The Vector3 make the transform rotate only on the Y axis,
    /// in other word, it acts like if the target is on the same plane as the transform.position.
    /// </summary>
    /// <param name="target">The point coordinate to look at.</param>
    /// <param name="yMin">The minimal value of the y angle (vertical axis) of the rotation.</param>
    /// <param name="yMax">The maximal value of the y angle (vertical axis) of the rotation.</param>
    /// <returns></returns>
    public static Vector3 LookAtYClamped_V3(this Transform transform, Vector3 target, float yMin, float yMax) {


        //Get a result similar to LookAtY();
        Quaternion quat = Quaternion.LookRotation(target.SetY(transform.position.y) - transform.position, Vector3.up);

        //Convert it to vector 3 rotation, in degrees.
        Vector3 eulers = quat.eulerAngles;

        //if yMin < 0, the call may probably use a range from ]-180; 180] instead of [0; 360[
        if (yMin < 0) {

            //make yMax greated than yMin, and lower than 180
            yMax = Mathf.Clamp(yMax, yMin + 1, 180);

            //In this case, if eulers.y is in the ]180;360[ range, it must be converted in its equivalent in the ]-180; 0[ range.
            if (eulers.y > 180) {
                //Since a Quatertion.eulerAngles will always return angles values in the range [0;360[, there is not need to check if the result is still highter than 180
                eulers.y -= 360;
            }
        }

        //clamp the angle between the min and max values.
        eulers.y = Mathf.Clamp(eulers.y, yMin, yMax);

        return eulers;
    }

    /// <summary>
    /// Return a Vector3, which can make a transform looking at the direction of the target position. The Vector3 make the transform rotate only on the X axis,
    /// in other word, it acts like if the target is in front of the transform, and only look up or down.
    /// </summary>
    /// <param name="target">The point coordinate to look at.</param>
    /// <param name="yMin">The minimal value of the x angle (left-right axis) of the rotation.</param>
    /// <param name="yMax">The maximal value of the x angle (left-right axis) of the rotation.</param>
    /// <returns></returns>
    public static Vector3 LookAtXClamped_V3(this Transform transform, Vector3 target, float yMin, float yMax) {

        //hypotenuse is the ray between transform.position and target.
        float adjacent = (transform.position - target.SetY(transform.position.y)).magnitude; //adjacent is horizontal distance between transform.position and target.
        float opposite = (transform.position.y - target.y); //opposite is vertical distance between transform.position and target.
        float result = Mathf.Atan((opposite / adjacent)) * Mathf.Rad2Deg;
        result = Mathf.Clamp(result, yMin, yMax);
        return transform.localEulerAngles.SetX(result);
    }

    /// <summary>
    /// Check if a collection contain at least one duplicate, but do not return duplicate value, nor the number of duplicate.
    /// </summary>
    /// <returns>Return true if contains duplicate.</returns>
    public static bool ContainsDuplicates<T>(this IEnumerable<T> enumerable) {
       
        //Create an HashSet, wich cannont contain duplicate.
        HashSet<T> hashSet = new HashSet<T>();
        //Try to add every instance of the enumerabl to the HashSet.
        //hashSet.Add() will return false if hashSet already contain the element.
        //so it return true, in Any member of the enumerable is a duplicate.
        return enumerable.Any(item => hashSet.Add(item)==false);
    }

    /// <summary>
    /// Return the number of occurence of a char in a string, case sensitive.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="c"></param>
    /// <returns></returns>
    public static int OccurenceOfChar(this string s, char c) {
        int a = 0;
        for (int i = 0; i < s.Length; i++) {
            if (s[i] == c) {
                a++;
            }
        }
        return a;
    }


    /// <summary>
    /// Return the number of occurence of a char in a string, case insensitive.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="c"></param>
    /// <returns>The number of occurence of the char.</returns>
    public static int OccurenceOfCharInsensitiv(this string s, char c) {
        s = s.ToUpperInvariant();
        c = char.ToUpperInvariant(c);
        int a = 0;
        for (int i = 0; i < s.Length; i++) {
            if (s[i] == c) {
                a++;
            }
        }
        return a;
    }


    /// <summary>
    /// Return if a Transform is a subchild of another.
    /// </summary>
    /// <param name="potentialParent"></param>
    /// <returns></returns>
    public static bool IsChildOrSubChildOf(this Transform transform, Transform potentialParent) {
        Transform transformToTest = transform.parent;
        do {
            if(transformToTest == potentialParent) {
                return true;
            }
            transformToTest = transformToTest.parent;
        } while (transformToTest != null) ;
        return false;
    }

}
